CREATE DATABASE flask_cinema;
USE flask_cinema;

CREATE USER 'Flagpole2271'@'localhost' IDENTIFIED BY 'AfXiu5qap6cwHrMLih!HDmd$Bwr9SdR%';

GRANT ALL PRIVILEGES ON flask_cinema.* TO 'Flagpole2271'@'localhost';

CREATE TABLE user (
	user_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	email VARCHAR(255) NOT NULL UNIQUE,
	password VARCHAR(255) NOT NULL
);