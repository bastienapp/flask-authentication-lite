import os

# variables globales de configuration de la base de données
MYSQL_HOST = os.getenv('DB_HOST')
MYSQL_USER = os.getenv('DB_USER')
MYSQL_PASSWORD = os.getenv('DB_PASSWORD')
MYSQL_DB = os.getenv('DB_NAME')
MYSQL_CURSORCLASS = 'DictCursor'  # pour générer des résultat en Dictionnaire
