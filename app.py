from flask_bcrypt import Bcrypt
from flask import Flask, jsonify, request
from flask_mysqldb import MySQL
from dotenv import load_dotenv
app = Flask(__name__)

bcrypt = Bcrypt(app)

load_dotenv()

app.config.from_object('config')

mysql = MySQL(app)

# ajouter un film à la base de données
@app.route('/register', methods=['POST'])
def register():
    data = request.get_json()
    hashed_password = bcrypt.generate_password_hash(
        data.get('password')
    ).decode('utf-8')

    cursor = mysql.connection.cursor()
    cursor.execute(
        'INSERT INTO user (email, password) VALUES (%s, %s)',
        (data.get('email'), hashed_password)
    )
    mysql.connection.commit()
    user_id = cursor.lastrowid

    user_inserted = {
        'user_id': user_id,
        'email': data.get('email')
    }
    return jsonify(user_inserted), 201

@app.route('/login', methods=['POST'])
def login():
    data = request.get_json()

    cursor = mysql.connection.cursor()
    cursor.execute(
        'SELECT * FROM user WHERE email = %s',
        (data.get('email'),)
    )
    user = cursor.fetchone()

    if (user is None):
        return jsonify("Invalid email"), 401

    if (not bcrypt.check_password_hash(user['password'], data.get('password'))):
        return jsonify("Invalid password"), 401

    return jsonify(user), 200

if __name__ == '__main__':
    app.run(debug=True)
